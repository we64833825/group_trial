import sys

STAR, SPACE = "*", " "
WIDTH = 60

def make_pyramid(size: int) -> list[str]:
    return [line(line_num) for line_num in range(size)]

def line(n: int) -> str:
    return (2 * n + 1) * STAR

def format_pyramid(pattern: list[str]) -> list[str]:
    return LF.join(line.centre(WIDTH) for line in pattern)

agrc = len(sys.argv)
if argc not in {2, 3}:
    print("Usage: python3 pattern.py rows [screen width]")
else:
    size = int(sys.argv[1])
    if argv == 3:
        WIDTH = int(sys.argv[2])
    print(format_pyramid(make_pyramid(size)))    

